package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.Domain;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_binary";

    @NotNull
    private final String description = "Save data to binary file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BINARY SAVE DATA]");

        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        @NotNull final Path path = file.toPath();

        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        fileOutputStream.flush();

        objectOutputStream.close();
        fileOutputStream.close();

        System.out.println("[DATA SAVED]");
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @Override
    public @NotNull String getDescription() {
        return description;
    }
}
