package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.Domain;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private final String name = "save_base64";

    @NotNull
    private final String description = "Save data to file as Base64 String";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BASE64 SAVE DATA]");
        @NotNull Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        @NotNull final Path path = file.toPath();
        Files.deleteIfExists(path);
        Files.createFile(path);

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NotNull byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull String base64 = new BASE64Encoder().encode(bytes);

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
        System.out.println("[DATA SAVED]");
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    @Override
    public @NotNull String getDescription() {
        return description;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
