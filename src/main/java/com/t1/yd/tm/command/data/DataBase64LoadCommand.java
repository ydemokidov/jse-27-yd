package com.t1.yd.tm.command.data;

import com.t1.yd.tm.dto.Domain;
import com.t1.yd.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "load_base64";

    @NotNull
    private final String description = "Load data from base64 file";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[BASE64 DATA LOAD]");

        @NotNull final byte[] base64Bytes = Files.readAllBytes(Paths.get(FILE_BASE64));
        @Nullable final String base64Str = new String(base64Bytes);
        @Nullable final byte[] bytes = new BASE64Decoder().decodeBuffer(base64Str);

        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        objectInputStream.close();
        byteArrayInputStream.close();

        setDomain(domain);

        System.out.println("[DATA LOADED]");
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public @NotNull String getDescription() {
        return description;
    }
}
