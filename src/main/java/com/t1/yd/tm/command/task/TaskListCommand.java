package com.t1.yd.tm.command.task;

import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task_list";

    @NotNull
    public static final String DESCRIPTION = "Show list of tasks";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));

        @NotNull final String sortType = TerminalUtil.nextLine();
        @NotNull final Sort sort = Sort.toSort(sortType);
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, sort);

        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
