package com.t1.yd.tm.command.data;

import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.dto.Domain;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    public static final String FILE_BINARY = "./data.bin";

    @NotNull
    public Domain getDomain() {
        @NotNull Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().add(domain.getUsers());
        serviceLocator.getProjectService().add(domain.getProjects());
        serviceLocator.getTaskService().add(domain.getTasks());
        serviceLocator.getAuthService().logout();
    }

}
